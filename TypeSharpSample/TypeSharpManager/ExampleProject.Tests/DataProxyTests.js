var __extends = this.__extends || function (d, b) {
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var ExampleProject;
(function (ExampleProject) {
    /// <reference path="..\ExampleProject\DataProxy.ts" />
    /// <reference path="..\tsUnit.ts" />
    (function (Tests) {
        var DataProxyTests = (function (_super) {
            __extends(DataProxyTests, _super);
            function DataProxyTests() {
                _super.apply(this, arguments);

            }
            return DataProxyTests;
        })(tsUnit.TestClass);
        Tests.DataProxyTests = DataProxyTests;        
    })(ExampleProject.Tests || (ExampleProject.Tests = {}));
    var Tests = ExampleProject.Tests;
})(ExampleProject || (ExampleProject = {}));
//@ sourceMappingURL=DataProxyTests.js.map
