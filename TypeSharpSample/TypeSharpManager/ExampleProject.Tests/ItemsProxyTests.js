var __extends = this.__extends || function (d, b) {
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var ExampleProject;
(function (ExampleProject) {
    /// <reference path="..\ExampleProject\ItemsProxy.ts" />
    /// <reference path="..\tsUnit.ts" />
    (function (Tests) {
        var ItemsProxyTests = (function (_super) {
            __extends(ItemsProxyTests, _super);
            function ItemsProxyTests() {
                _super.apply(this, arguments);

            }
            return ItemsProxyTests;
        })(tsUnit.TestClass);
        Tests.ItemsProxyTests = ItemsProxyTests;        
    })(ExampleProject.Tests || (ExampleProject.Tests = {}));
    var Tests = ExampleProject.Tests;
})(ExampleProject || (ExampleProject = {}));
//@ sourceMappingURL=ItemsProxyTests.js.map
