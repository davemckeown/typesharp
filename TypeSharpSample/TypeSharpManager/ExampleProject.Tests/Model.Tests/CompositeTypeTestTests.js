var __extends = this.__extends || function (d, b) {
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var ExampleProject;
(function (ExampleProject) {
    (function (Tests) {
        (function (Model) {
            /// <reference path="..\..\ExampleProject\Model\CompositeTypeTest.ts" />
            /// <reference path="..\..\tsUnit.ts" />
            (function (Tests) {
                var CompositeTypeTestTests = (function (_super) {
                    __extends(CompositeTypeTestTests, _super);
                    function CompositeTypeTestTests() {
                        _super.apply(this, arguments);

                    }
                    return CompositeTypeTestTests;
                })(tsUnit.TestClass);
                Tests.CompositeTypeTestTests = CompositeTypeTestTests;                
            })(Model.Tests || (Model.Tests = {}));
            var Tests = Model.Tests;
        })(Tests.Model || (Tests.Model = {}));
        var Model = Tests.Model;
    })(ExampleProject.Tests || (ExampleProject.Tests = {}));
    var Tests = ExampleProject.Tests;
})(ExampleProject || (ExampleProject = {}));
//@ sourceMappingURL=CompositeTypeTestTests.js.map
