/// <reference path="ExampleProject.Model.Data.d.ts" />

module ExampleProject.Model.Data {

	export interface IDataItem {
	
		DataField: string;
	
		DataValue: any;
	
	}
	
}