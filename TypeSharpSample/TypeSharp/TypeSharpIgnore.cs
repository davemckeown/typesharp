﻿using System;

namespace TypeSharp
{
    [System.AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class TypeSharpIgnore : System.Attribute
    {
    }
}

