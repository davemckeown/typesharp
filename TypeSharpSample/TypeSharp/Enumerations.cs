﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TypeSharp
{

    public enum TypeScriptType
    {
        Any,
        String,
        Number,
        Date,
        Void,
    }

}
