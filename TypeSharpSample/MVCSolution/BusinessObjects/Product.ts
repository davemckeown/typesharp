/// <reference path="BusinessObjects.d.ts" />

module BusinessObjects {

	/**
	* Product
	*/
	export class Product {
	
		/**
		* Gets or sets the product name
		*/
		Name: string;
	
		/**
		* Gets or sets the unit price
		*/
		UnitPrice: number;
	
		/**
		* Gets or sets the Description
		*/
		Description: string;
	
	}
	
}